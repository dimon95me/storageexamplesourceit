package com.example.student.sharedpreferencesourceit;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesManager {
    public static final String EDIT_NAME = "EditName";
    public static final String PREF_NAME = "PrefName";


   public static void saveName(Context context, String value){
       SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
       SharedPreferences.Editor editor = sharedPreferences.edit();
       editor.putString(EDIT_NAME, value);
       editor.apply();
   }

   public static String getValue(Context context){
       SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);

       return sharedPreferences.getString(EDIT_NAME, "Unknown");

   }
}
