package com.example.student.sharedpreferencesourceit;

import android.os.Environment;

public class ExternalStorageUtil {

    public boolean isExternalStorageWritable(){
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state))
        {
            return true;
        }
        return false;
    }

    public boolean isExternalStorageReadable(){
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) || Environment.MEDIA_MOUNTED_READ_ONLY){
    return true;
        }

        return false;
    }
}
