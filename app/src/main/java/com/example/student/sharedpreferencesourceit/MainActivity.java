package com.example.student.sharedpreferencesourceit;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST = 112;

    @BindView(R.id.text_for_saving)
    EditText text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        String[] PERMSSIONS  = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
        ActivityCompat.requestPermissions(this, PERMSSIONS, REQUEST);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode){
            case REQUEST: {
                if (grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){

                }
            }
        }
    }

    @OnClick(R.id.save)
    public void onClickSave(){
//        SharedPreferencesManager.saveName(getApplicationContext(), text.getText().toString() );
//        text.setText("");

        InternalStorageManager.saveValue(getApplicationContext(), text.getText().toString());
        text.setText("");
    }

    @OnClick(R.id.restore)
    public void onClickRestore(){
//        text.setText(SharedPreferencesManager.getValue(getApplicationContext()));
        text.setText(InternalStorageManager.getValue(getApplicationContext()));
    }
}
