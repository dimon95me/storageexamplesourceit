package com.example.student.sharedpreferencesourceit;

import android.content.Context;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class InternalStorageManager {
    private static final String FILE_NAME = "file_name";

    public static void saveValue(Context context, String value){
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            fileOutputStream.write(value.getBytes());
        } catch (FileNotFoundException e) {
            Toast.makeText(context, "File does not found", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (fileOutputStream!=null){
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    Toast.makeText(context, "File output stream error", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public static String getValue(Context context){
        String line = null;
        FileInputStream fileInputStream = null;
        StringBuilder stringBuilder = null;
        try {
            fileInputStream = context.openFileInput(FILE_NAME);
            BufferedReader reader = new BufferedReader(new InputStreamReader(fileInputStream));
            stringBuilder = new StringBuilder();

            while ((line = reader.readLine())!=null){
                stringBuilder.append(line);
            }

        } catch (FileNotFoundException e) {
            Toast.makeText(context, "File input stream not found", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fileInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return stringBuilder.toString();
    }
}
